import Vue from 'vue'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueSweetalert2 from 'vue-sweetalert2';
import VueRouter from 'vue-router'

Vue.use(VueRouter)

Vue.config.productionTip = false
Vue.use(VueSweetalert2);
new Vue({
  render: h => h(App),
}).$mount('#app')
