# ejercicio_clevers_services

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Link del proyecto hosteado
https://clever-services-evaluation.web.app/

# Si ingresa al link puede hacer uso del proyecto, de lo contrato es necesario agregar el archivo .env con las siguientes varibales, agregando sus credenciales.


VUE_APP_API_KEY=
VUE_APP_AUTH_DOMAIN=
VUE_APP_PROJECT_ID=
VUE_APP_STORAGE_BUCKET=
VUE_APP_MESSAGING_SENDER_ID=

